package workshop.gdg.kotlinandroid.extensions

import android.content.Context
import android.content.Intent

inline fun <reified T : Any> Context.launchActivity(
        vararg options: Pair<String, String>) {

    val intent = newIntent<T>(this)
    options.forEach {
        intent.putExtra(it.first, it.second)
    }
    startActivity(intent)
}

inline fun <reified T : Any> newIntent(context: Context): Intent =
        Intent(context, T::class.java)