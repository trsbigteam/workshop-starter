package workshop.gdg.kotlinandroid.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.product_list_item.view.*
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.presentation.model.Product

class ProductListAdapter(private val items: ArrayList<Product>, private val context: Context, private val listener: OnItemClickedListener) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.product_list_item, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = items[position]
        holder.bind(product, listener)
    }
}


class ViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val productImage: ImageView = view.productImage
    val productName: TextView = view.productName
    val productBrands: TextView = view.productBrand
    val productGrade: TextView = view.productGrade
    val productGraColor: ImageView = view.productGradeColor
    val productBarCode: TextView = view.productBarCode

    fun bind(product: Product, listener: OnItemClickedListener) {
        //TODO Exercice 2 - Associer les informations du produit avec les données du view holder
    }
}

interface OnItemClickedListener {
    fun onItemClicked(product: Product)
}