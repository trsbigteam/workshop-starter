package workshop.gdg.kotlinandroid.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import workshop.gdg.kotlinandroid.data.database.model.ProductEntity

@Dao
interface ProductDataDao {

    @Query("SELECT * from productEntity")
    fun getAll(): List<ProductEntity>


    @Query("SELECT * FROM productEntity WHERE id = :id ")
    fun getProductById(id: Long): ProductEntity?

    @Insert(onConflict = REPLACE)
    fun insert(productEntity: ProductEntity)

    @Query("DELETE from productEntity")
    fun deleteAll()
}