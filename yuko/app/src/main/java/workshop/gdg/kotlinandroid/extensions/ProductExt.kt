package workshop.gdg.kotlinandroid.extensions

import android.content.Context
import android.widget.ImageView
import androidx.core.content.ContextCompat
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.data.database.model.ProductEntity
import workshop.gdg.kotlinandroid.presentation.GlideApp
import workshop.gdg.kotlinandroid.presentation.model.Product

fun ProductEntity.toPresentation() =
    Product(id, name, imageIngredientsThumbUrl, nutritionGradeFr, allergens, imageUrl, brands)

fun Product.getGradeColor(context: Context) = with(nutritionGradeFr) {
    if (nutritionGradeFr != null) {
        when (this?.toLowerCase()) {
            "a" -> R.color.a
            "b" -> R.color.b
            "c" -> R.color.c
            "d" -> R.color.d
            "e" -> R.color.e
            else -> R.color.not_available
        }
    } else {
        R.color.not_available
    }.run { ContextCompat.getColor(context, this) }

}

fun Product.getGrade(context: Context) = with(nutritionGradeFr) {
    if (nutritionGradeFr != null) {
        when (this?.toLowerCase()) {
            "a" -> R.string.a
            "b" -> R.string.b
            "c" -> R.string.c
            "d" -> R.string.d
            "e" -> R.string.e
            else -> R.string.not_available
        }
    } else {
        R.string.not_available
    }.run {  context.getString(this) }
}

fun Product.loadProductImage(view: ImageView) =
    GlideApp.with(view).load(imageUrl ?: R.drawable.not_available).into(view)
