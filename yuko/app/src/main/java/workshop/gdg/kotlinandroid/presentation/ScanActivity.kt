package workshop.gdg.kotlinandroid.presentation

import android.os.Bundle
import kotlinx.android.synthetic.main.scan_activity.*
import me.dm7.barcodescanner.zbar.Result
import me.dm7.barcodescanner.zbar.ZBarScannerView
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.extensions.launchActivity
import workshop.gdg.kotlinandroid.presentation.ProductDetailsActivity.Companion.EXTRA_PRODUCT_ID


class ScanActivity : BaseActivity(), ZBarScannerView.ResultHandler {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scan_activity)
    }

    override fun onResume() {
        super.onResume()
        //TODO Exercice 3 - start camera
    }

    override fun onPause() {
        super.onPause()
       //TODO Exercice 3 - stop camera
    }

    override fun handleResult(result: Result?) {
        //TODO Exercice 3 - Gérer le résultat et démarrer la bonne activité
    }
}

