package workshop.gdg.kotlinandroid.presentation.model

import android.content.Context
import androidx.core.content.ContextCompat
import android.widget.ImageView
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.data.database.model.ProductEntity
import workshop.gdg.kotlinandroid.presentation.GlideApp

data class Product constructor(
    val code: Long,
    val name: String,
    var imageIngredientsThumbUrl: String? = null,
    var nutritionGradeFr: String? = null,
    var allergens: String? = null,
    var imageUrl: String? = null,
    var brands: String? = null
)
