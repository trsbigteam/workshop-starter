package workshop.gdg.kotlinandroid.data.repository

import android.app.Application
import workshop.gdg.kotlinandroid.presentation.model.Product

const val BASE_URL = "https://fr.openfoodfacts.org/api/v0/produit/"

class ProductRepositoryImpl(val application: Application) : ProductRepository {

    override suspend fun getProduct(id: String): Product? {
        //TODO Exercice 5 - appeler la méthode retrieveProductFromWebAndCache en asynchrone
        return null
    }

    override suspend fun getAllProduct(): List<Product> {
        //TODO Exercice 5 -  :récupérer la liste en base de données
        return listOf(Product(-1, "test", "test"))
    }


    private fun retrieveProductFromWebAndCache(id: String): Product? {
        //"https://fr.openfoodfacts.org/api/v0/produit/111.json"
        //TODO Exercice 5 -  : créer l'url
        //TODO Exercice 5 -  : faire le parsing
        //TODO Exercice 5 -  : insertion en base de données
        return null
    }
}