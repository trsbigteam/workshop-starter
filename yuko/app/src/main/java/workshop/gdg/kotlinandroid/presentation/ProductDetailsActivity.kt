package workshop.gdg.kotlinandroid.presentation

import android.os.Bundle
import workshop.gdg.kotlinandroid.R
class ProductDetailsActivity : BaseActivity() {

    val context = this@ProductDetailsActivity

    companion object {
        const val EXTRA_PRODUCT_ID = "EXTRA_PRODUCT_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_details_activity)
        val productId = intent?.extras?.get(EXTRA_PRODUCT_ID) as String
        getProduct(productId)
    }

    private fun getProduct(id: String) {
       //TODO Exercice 4 - récuperer les infos sur le produit
    }
}