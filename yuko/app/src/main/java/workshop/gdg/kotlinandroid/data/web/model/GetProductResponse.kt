package workshop.gdg.kotlinandroid.data.web.model

import com.google.gson.annotations.SerializedName
import workshop.gdg.kotlinandroid.data.database.model.ProductEntity

class GetProductResponse {

    @SerializedName("code")
    var code: String? = null
    @SerializedName("product")
    var productResponse: ProductResponse? = null
    @SerializedName("status")
    var status: Int = 0

    fun toEntity(): ProductEntity? =
        code?.let { id ->
            productResponse?.let {
                ProductEntity(
                    id.toLong(),
                    it.imageIngredientsThumbUrl,
                    it.nutritionGradeFr,
                    it.name,
                    it.allergens,
                    it.imageUrl,
                    it.brands
                )
            }
        }
}
