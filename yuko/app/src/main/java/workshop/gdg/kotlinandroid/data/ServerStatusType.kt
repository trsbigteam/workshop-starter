package workshop.gdg.kotlinandroid.data

enum class ServerStatusType(val statusId : Int){
    OK_STATUS(1),
    UNKNOW(-1);

    companion object {
        fun fromId(serverId: Int): ServerStatusType {
            return values().find { it.statusId == serverId } ?: UNKNOW
        }
    }
}